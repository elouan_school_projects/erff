# erff

### Contenu :

```
├── documentation                // Ensemble des documentation
│   ├── sources                  // Sources utilisés au cours du projet        
|   |    └── ...
│   ├── backups                  // Backups  de la carte Raspberry Pi
|   |    └── config.tar.gz       // dossier de configuration des logiciels zigbee2mqtt, mosquitto et openhab
|   └── report                   // Rapport Final
|        └── ...
├── src                          // Ensemble du code
│   ├── docker                   // Codes de la gateway
|   |    └── ...
|   └── implementation_fpga      // Codes du FPGA
|        └── ...
└── README.md
```

<br/>
