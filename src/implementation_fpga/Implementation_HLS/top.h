#ifndef __TOP_H__
#define __TOP_H__
#define N 250
#define K 81
#define f_prec 1000
#define f_max 40000


#include "ap_fixed.h"

typedef ap_fixed<18, 6> sample16_f;

sample16_f mult2vec(sample16_f vec1[N], const sample16_f vec2[N]);
void projectSignal(sample16_f signal_I[N],sample16_f signal_Q[N], sample16_f coeff_I[K], sample16_f coeff_Q[K]);
void top(sample16_f *coeff_I, sample16_f *coeff_Q, sample16_f * s_I, sample16_f* s_Q);


#endif
