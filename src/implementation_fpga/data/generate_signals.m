clear all;
close all;
clc;

rng(42);

% Param�tres

%%% Puissance du bruit
var_n=0.01; % SNR=20dB

%%% Nombre de points du signal
L=10000;

%%% Ordre de la modulation M-PSK
M=4;

%%% Nombre de symboles M-PSK
Nsymb=300;

%%% Facteur de sur-�chantillonnage
OS=10;

%%% Taille de la fen�tre glissante du d�tecteur
N=256;

%%% Seuil pour la loi normal centr�e-r�duite
s=3.71902; % 99.99%

% G�n�ration du bruit

%%% G�n�ration du bruit

noise=[1 1i]*randn(2, L)*sqrt(var_n/2);

%%% Ecriture des �chantillons dans un fichier

fileID=fopen('noise.txt', 'w');
for i=1:length(noise)
    fprintf(fileID, '%f %f\n', [real(noise(i)) imag(noise(i))]);
end
fclose(fileID);

%%% Affichage du bruit g�n�r�

figure();
plot(real(noise));
hold on;
plot(imag(noise));
title('Temporal aspect of noise');
legend('Real Part','Imag part');

%%% Estimation de la puissance du bruit

var_n_=mean(abs(noise).^2);

disp(['Noise variance estimation: ' num2str(var_n_)]);

% Generation du signal

%%% G�n�ration du signal M-PSK

symb=randi([0 M-1], 1, Nsymb);
s_mod=kron(pskmod(symb, M), ones(1, OS));

begin=floor(1000+rand()*100);
signal=zeros(1, L);
signal(begin:begin+Nsymb*OS-1)=s_mod;

%%% G�n�ration du signal bruit�

r=signal+noise;

%%% Ecriture du fichier d'entr�e

fileID=fopen('input.txt', 'w');
for i=1:length(r)
    fprintf(fileID, '%f %f\n', [real(r(i)) imag(r(i))]);
end
fclose(fileID);

%%% Affichage du signal bruit�

figure();
subplot(1,2,1);
plot(abs(r).^2);
title('Energy of signal');
subplot(1,2,2);
plot(real(r));
hold on;
plot(imag(r));
title('Temporal aspect of signal');
legend('Real part', 'Imag part');

% D�tection de signal

%%% Calcul du signal de d�tection

E=conv(ones(1, N), abs(r).^2);
h=zeros(1, L);
s_E=s*sqrt(N*var_n^2)+N*var_n;
h(E > s_E)=1;

%%% Ecriture du fichier de sortie

fileID=fopen('output.txt', 'w');
for i=1:L
    fprintf(fileID, '%f %f\n', [E(i) h(i)]);
end
fclose(fileID);

%%% Affichage du signal de d�tection

figure();
plot(abs(r).^2);
hold on;
plot(h);
title('Energy detection');
legend('Energy', 'Detection');