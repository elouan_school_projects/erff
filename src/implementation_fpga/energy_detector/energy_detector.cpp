#include "energy_detector.h"

float16_t compute_energy(complex16_t in) {
	float16_t energy_acc = 0;
	static complex16_t sliding_window[WINDOW_SIZE] = { };

	for (int i = WINDOW_SIZE - 1; i >= 0; i--) {
		if (i == 0) {
			sliding_window[i] = in;
		} else {
			sliding_window[i] = sliding_window[i - 1];
		}

		energy_acc += (sliding_window[i].real() * sliding_window[i].real()
				+ sliding_window[i].imag() * sliding_window[i].imag());
	}
	return energy_acc;
}

bool is_noise(float16_t energy, float16_t energy_threshold) {
	if (energy >= energy_threshold) {
		return false;
	} else {
		return true;
	}
}

float16_t compute_energy_threshold(float16_t alpha, float16_t noise_strength) {
	float16_t threshold_s = -1;

	// TODO: use the centered normal distribution table to support every values;
	if (alpha == 0.05) {
		threshold_s = 1.64485;
	} else if (alpha == 0.01) {
		threshold_s = 2.3263;
	} else {
		return 0;
	}

	return threshold_s; //* std::sqrt(WINDOW_SIZE * noise_strength * noise_strength) + WINDOW_SIZE * noise_strength;
}

float16_t compute_noise_strength(complex16_t in[DATA_SIZE]) {
	float16_t sum = 0;
	for (int i = 0; i < DATA_SIZE; i++) {
		sum += (in[i].real() * in[i].real());
	}

	return 1 / DATA_SIZE * sum;
}
