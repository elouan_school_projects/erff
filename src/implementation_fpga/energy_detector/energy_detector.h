#include "ap_fixed.h"
#include "hls_dsp.h"

#include <complex>

/* CONSTANTS */
const char FLOATING_POINT_SIZE = 16;
const char ABOVE_DECIMAL_POINT = 4;

#define DATA_SIZE 10000
#define WINDOW_SIZE 256

/* TYPES */
typedef ap_fixed<32, 16> float16_t;
typedef std::complex<float16_t> complex16_t;

/* FUNCTIONS DEFINITION */
float16_t compute_energy(complex16_t in);

bool is_noise(float16_t energy, float16_t energy_threshold);

float16_t compute_energy_threshold(float16_t alpha, float16_t noise_strength);

float16_t compute_noise_strength(complex16_t in[DATA_SIZE]);
