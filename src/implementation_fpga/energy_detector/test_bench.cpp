#include "energy_detector.h"
#include <fstream>
#include <iostream>

int main() {
	float16_t alpha = 0.0001;
	float16_t noise_strength = 0.001;
	float16_t energy_threshold = 3.155043200000000;

	complex16_t signal[DATA_SIZE];
	complex16_t signal_energies[DATA_SIZE];
	complex16_t signal_usefullness[DATA_SIZE];

	float16_t val;
	float16_t energy;

	// Read input file
	int i = 0;
	std::ifstream input_file("../../../../implementation_fpga/data/input.txt");
	for (int i = 0; i < DATA_SIZE; i++) {
		input_file >> val;
		signal[i].real(val);
		input_file >> val;
		signal[i].imag(val);

		// Execute energy detection if the window is ready (WINDOW_SIZE values are stored)
		energy = compute_energy(signal[i]);
		signal_energies[i] = energy;

		if (is_noise(energy, energy_threshold)) {
			signal_usefullness[i] = 0;
		} else {
			signal_usefullness[i] = 1;
		}

		std::cout << "energy: " << signal_energies[i] << ", is_noise: "
				<< signal_usefullness[i] << std::endl;
	}

	// Write output file
	std::ofstream output_file(
			"../../../../implementation_fpga/data/computed_output.txt");
	for (int i = 0; i < DATA_SIZE; i++) {
		output_file << signal_energies[i].real() << " " << signal_usefullness[i].real()
				<< std::endl;
	}



	return 0;
}
