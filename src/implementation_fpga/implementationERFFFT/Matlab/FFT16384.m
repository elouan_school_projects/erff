clear all
close all;
clc;

rng(42);

Fs=5e6;
Ts=1/Fs;
Nfft=2^14;
M=4;

SNRdB=30;

load('preambleSmall.mat');

df=7500; % 3 ppm

N=length(preamble);
t=0:Ts:(N-1)*Ts;

formatSpec='%.3f';

r=awgn(preamble.*exp(2i*pi*df*t'), SNRdB);

f=0:Fs/Nfft:Fs-Fs/Nfft;
y=r.^M;
Y=fft(y, Nfft);
[~, index]=max(Y);

figure();
plot(f, abs(Y));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');
title(['Spectral aspect: ' num2str(f(index)/M) ' (Hz)']);

fileID=fopen('toCopy.txt', 'w');
fprintf(fileID, ['#define N ' num2str(N) '\n']);
fprintf(fileID, ['#define M ' num2str(M) '\n']);
fclose(fileID);

fileID=fopen('input.txt', 'w');
for i=1:length(r)
    I=real(r(i));
    Q=imag(r(i));
    fprintf(fileID, "%f %f\n", I, Q);
end
fclose(fileID);


disp('Write testbench signals in test_signal.h');

fileID=fopen('test_signal.h', 'w');
fprintf(fileID, ['signal16f r[' num2str(N) ']={']);
for i=1:N-1
    fprintf(fileID, [num2str(r(i), formatSpec) ',']);
end
fprintf(fileID, [num2str(r(end), formatSpec) '};\n']);
fprintf(fileID, ['sample16f r_I[' num2str(N) ']={']);
for i=1:N-1
    fprintf(fileID, [num2str(real(r(i)), formatSpec) ',']);
end
fprintf(fileID, [num2str(real(r(end)), formatSpec) '};\n']);
fprintf(fileID, ['sample16f r_Q[' num2str(N) ']={']);
for i=1:N-1
    fprintf(fileID, [num2str(imag(r(i)), formatSpec) ',']);
end
fprintf(fileID, [num2str(imag(r(end)), formatSpec) '};\n']);
fclose(fileID);