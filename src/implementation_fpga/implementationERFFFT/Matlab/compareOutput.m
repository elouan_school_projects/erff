clear all
close all;
clc;

Fs=5e6;
Ts=1/Fs;
Nfft=2^14;
M=4;

SNRdB=30;

load('preambleSmall.mat');

df=7500; % 3 ppm

N=length(preamble);
t=0:Ts:(N-1)*Ts;

fileID=fopen('input.txt', 'r');
r=([1 1i]*fscanf(fileID, "%f %f", [2 Inf])).';
fclose(fileID);

f=0:Fs/Nfft:Fs-Fs/Nfft;
y=r.^M;
Y=fft(y, Nfft);

fileID=fopen('output.txt', 'r');
Y_vivado=2^11*([1 1i]*fscanf(fileID, "%f %f", [2 Inf])).'; % A expliquer
fclose(fileID);

error=Y-Y_vivado;
normY=norm(Y)^2;
error_MSE=norm(error)^2;

disp(['Mean Square Error (MSE): ' num2str(error_MSE)]);
disp(['Mean Square Error (MSE): ' num2str(100.0*error_MSE/normY) '%']);

figure();
subplot(1,2,1);
plot(f, abs(Y));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');
subplot(1,2,2);
plot(f, abs(Y_vivado));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');

figure();
plot(f, abs(Y));
hold on;
plot(f, abs(Y_vivado));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');
title('Frequency Comparison');
legend('Theoretical','Vivado');

figure();
subplot(1,2,1);
plot(f, real(Y));
hold on;
plot(f, real(Y_vivado));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');
title('Frequency Comparison (Real part)');
legend('Theoretical','Vivado');
subplot(1,2,2);
plot(f, imag(Y));
hold on;
plot(f, imag(Y_vivado));
xlabel('Frequency (Hz)');
ylabel('Amplitude/frequency');
title('Frequency Comparison (Real part)');
legend('Theoretical','Vivado');
