#include <iostream>
#include <fstream>
#include "top.h"

int main(void){
	int i;
	cmpxDataIn r[N];
	cmpxDataOut coeffs[FFT_LENGTH];
	ifstream myInputFile;
    ofstream myOutputFile;
    bool error;
    bool IorQ;
    sample16f val;

    // Read input file
    i=0;
    IorQ=true;
    myInputFile.open("../../../../Matlab/input.txt");
    while (myInputFile >> val){
    	if (IorQ == true){
    		r[i].real(val);
    		IorQ=false;
    	}
    	else {
    		r[i].imag(val);
    		IorQ=true;
    		i++;
    	}
    }

    for (i=0; i<N; i++){

    }

    // Execute frequency estimation
    top(1, r, coeffs, &error);

    // Write result in output file
    myOutputFile.open("../../../../Matlab/output.txt");
    if (error == true){
    	cout << "Overflow during execution !" << std::endl;
    }
    else{
    	cout << "No overflow during execution" << std::endl;
    }
    for (int i=0; i<FFT_LENGTH; i++){
    	myOutputFile << std::real(coeffs[i]) << " " << std::imag(coeffs[i]) << std::endl;
    }
    myOutputFile.close();
    return 0;
}
