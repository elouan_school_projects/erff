
#include "top.h"

void dummy_proc_fe(bool direction, config_t *config, cmpxDataIn in[N], cmpxDataIn out[N]) {
    int i;
    config->setDir(direction);
    config->setSch(0x2AB);
    for (i = 0; i < N; i++)
        out[i] = in[i];
}

void dummy_proc_be(status_t *status_in, bool *ovflo, cmpxDataOut in[FFT_LENGTH], cmpxDataOut out[FFT_LENGTH]) {
    int i;
    for (i = 0; i < FFT_LENGTH; i++)
        out[i] = in[i];
    *ovflo = status_in->getOvflo() & 0x1;
}

void suppressModulation(cmpxDataIn in[N], cmpxDataIn out[FFT_LENGTH]) {
    int i;
    sample16f I, Q;
    sample16f tmpI, tmpQ;
    for (i = 0; i < N; i++) {
        //#pragma HLS unroll
        out[i] = std::pow(in[i], M);
    }
    for (i = N; i < FFT_LENGTH; i++) {
        //#pragma HLS unroll
        out[i] = 0.0;
    }
}

void top(bool direction, cmpxDataIn in[N], cmpxDataOut out[FFT_LENGTH], bool *ovflo) {
#pragma HLS interface ap_hs port = direction
#pragma HLS interface ap_fifo depth = 1 port = ovflo
#pragma HLS interface ap_fifo depth = 250 port = in
#pragma HLS interface ap_fifo depth = FFT_LENGTH port = out
#pragma HLS data_pack variable = in
#pragma HLS data_pack variable = out
#pragma HLS dataflow
    complex<sample16f> tmp_in[N];
    complex<sample16f> tmp_proc[FFT_LENGTH];
    complex<sample16f> tmp_out[FFT_LENGTH];
    config_t fft_config;
    status_t fft_status;
    dummy_proc_fe(direction, &fft_config, in, tmp_in);
    suppressModulation(tmp_in, tmp_proc);
    hls::fft<config1>(tmp_proc, tmp_out, &fft_status, &fft_config);
    dummy_proc_be(&fft_status, ovflo, tmp_out, out);
}
