#include "ap_fixed.h"
#include "hls_fft.h"

#define N 250
#define M 4

// configurable params
const char FFT_INPUT_WIDTH                     = 16;
const char FFT_OUTPUT_WIDTH                    = FFT_INPUT_WIDTH;
const char FFT_CONFIG_WIDTH                    = 16;
const char FFT_NFFT_MAX                        = 14;
const int  FFT_LENGTH                          = 1 << FFT_NFFT_MAX;


#include <complex>
using namespace std;

struct config1 : hls::ip_fft::params_t {
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = FFT_NFFT_MAX;
};

typedef hls::ip_fft::config_t<config1> config_t;
typedef hls::ip_fft::status_t<config1> status_t;

typedef ap_fixed<FFT_INPUT_WIDTH, 1> sample16f;
typedef std::complex<sample16f> signal16f;

//typedef ap_fixed<FFT_INPUT_WIDTH,1> data_in_t;
//typedef ap_fixed<FFT_OUTPUT_WIDTH,FFT_OUTPUT_WIDTH-FFT_INPUT_WIDTH+1> data_out_t;
typedef signal16f cmpxDataIn;
typedef signal16f cmpxDataOut;

void dummy_proc_fe(
    bool direction,
    config_t* config,
    cmpxDataIn in[N],
    cmpxDataIn out[N]);

void dummy_proc_be(
    status_t* status_in,
    bool* ovflo,
    cmpxDataOut in[FFT_LENGTH],
    cmpxDataOut out[FFT_LENGTH]);

void suppressModulation(
	cmpxDataIn in[N],
	cmpxDataIn out[FFT_LENGTH]);

void top(
    bool direction,
    cmpxDataIn in[N],
    cmpxDataOut out[FFT_LENGTH],
    bool* ovflo);
